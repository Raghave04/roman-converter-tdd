package com.thoughtworks.vapasi;

import java.util.HashMap;
import java.util.Map;


public class RomanConverter {

    public static Integer convertRomanToArabicNumber(String roman)
    {
        HashMap<String,Integer> romanMap = new HashMap();
        romanMap.put("I",1);
        romanMap.put("IV",4);
        romanMap.put("V",5);
        romanMap.put("IX",9);
        romanMap.put("X",10);

        int num = 0;
        int romanLength=roman.length();

        for(int i=0;i<romanLength;) {
            if(romanMap.containsKey(roman.substring(i, i+1))) {
                if(i+2<=roman.length()&&romanMap.containsKey(roman.substring(i, i+2))) {
                    num=num+romanMap.get(roman.substring(i,i+2));
                    i=i+2;
                }else {
                    num=num+romanMap.get(roman.substring(i,i+1));
                    i++;
                }
            }
        }

        if(num<=0)
            return -1;

        return num;
    }

    public static void main(String[] args) {

        String inputRoman = "III";
        Integer convertedNumber=0;
        convertedNumber = convertRomanToArabicNumber(inputRoman);
        System.out.println("The conversion for roman number " + inputRoman + " is " + convertedNumber);
    }

}
