package com.thoughtworks.vapasi;

public class Square extends Rectangle{

    private float side;

    public Square(float side) {
        super();
        this.side=side;
    }

    @Override
    public float calculateArea() {
        return side*side;
    }

    @Override
    public float calculatePerimeter() { return 4*(side); }

}
