package com.thoughtworks.vapasi;

public class Triangle implements Geometry{

    private float length;
    private float height;
    private float base;

    public Triangle(float length, float height, float base) {
        this.length=length;
        this.height=height;
        this.base=base;
    }

    @Override
    public float calculateArea() {

        float semiPerimeter=(length + height + base)/2;

        return (float) Math.sqrt(semiPerimeter * (semiPerimeter-length) * (semiPerimeter-height) * (semiPerimeter-base));
    }

    @Override
    public float calculatePerimeter() {
        return length + height + base;
    }
}
