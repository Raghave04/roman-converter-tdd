package com.thoughtworks.vapasi;

import java.util.Random;

public class Toss {
    public String initializeToss() {
        Random random=new Random();
        int chance= random.nextInt(2);
        if(chance==1)
            return "heads";
        else
            return"tails";
    }
}
