package com.thoughtworks.vapasi;

import java.util.Objects;
import java.util.Random;

public class Probability{

    private double valueOfEvent1;
    private double valueOfEvent2;

    public Probability(double valueOfEvent1, double valueOfEvent2) {
        this.valueOfEvent1=valueOfEvent1;
        this.valueOfEvent2=valueOfEvent2;
    }


/*    public double initializeEvent() {
        Random random=new Random();
        int chance= random.nextInt(1);
        if(chance==1)
            return 1.0;
        else
            return 0.1;
    }*/

    //Probability of an event
    public double getProbabilityOfAnEvent()
    {
        return valueOfEvent1;
    }

    //Probability of occurrence of Event1 and Event2 independently
    public double getProbabilityOfTwoCombinedEvents()
    {
        return valueOfEvent1 * valueOfEvent2;
    }

    //Probability of occurrence of event1 or Event2
    public double getProbabilityOfEitherOneEvent()
    {
        return valueOfEvent1 + valueOfEvent2;
    }

    //Checking equity of two probabilities
    public boolean getEquityOfTwoEvents()
    {
        if ((boolean) (valueOfEvent1==valueOfEvent2)) return true;
        return false;
    }

}

