package com.thoughtworks.vapasi;

public interface Geometry {

    public float calculateArea() ;

    public float calculatePerimeter() ;

}
