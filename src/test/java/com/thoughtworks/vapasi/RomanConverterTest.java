package com.thoughtworks.vapasi;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class RomanConverterTest {

    private RomanConverter romanConvertor;
    private String romanNumber;
    private Integer expected;

    @BeforeEach
    void setUp() {
        romanConvertor = new RomanConverter();
    }

    @ParameterizedTest
    @MethodSource("romanToArabic")
    public void shouldConvertToRoman(String romanNumber, Integer expected) {
        Integer actual=romanConvertor.convertRomanToArabicNumber(romanNumber);

        System.out.println("Parameterized Number is : " + romanNumber);
        assertEquals(expected,actual, "not matching");
    }

    public static Collection romanToArabic() {
        return Arrays.asList(new Object[][] {
                { "I", 1 },
                { "III", 3 },
                { "IV", 4 },
        });
    }

    @Test
    public void romanConversionChecker() {

        Integer actual=romanConvertor.convertRomanToArabicNumber(romanNumber);

        System.out.println("Parameterized Number is : " + romanNumber);
        assertEquals(expected,actual);
    }
}
