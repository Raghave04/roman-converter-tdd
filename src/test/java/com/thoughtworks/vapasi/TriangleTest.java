package com.thoughtworks.vapasi;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TriangleTest {

    @Test
    void shouldCalculateArea() {

        float length=5.0f;
        float height=5.0f;
        float base=6.0f;

        Geometry Triangle = new Triangle(length,height,base);
        assertEquals(12.0f,Triangle.calculateArea(),"Wrong output");

    }

    @Test
    void shouldCalculatePerimeter() {

        float length=5.0f;
        float height=5.0f;
        float base=6.0f;

        Geometry Triangle = new Triangle(length,height,base);
        assertEquals(16.0f,Triangle.calculatePerimeter(),"Wrong output");

    }


}
