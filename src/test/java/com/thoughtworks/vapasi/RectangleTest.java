package com.thoughtworks.vapasi;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RectangleTest {



    @Test
    void shouldCalculateAreaGivenValidInput(){
        float length= 10.0f;
        float breadth= 5;
        float expectedArea=50;

        Geometry rectangle=new Rectangle(length,breadth);
        float actualArea=rectangle.calculateArea();

        assertEquals(expectedArea,actualArea,"Wrong calculation");

    }

    @Test
    void shouldCalculateAPerimeterGivenValidInput(){
        float length= 10.0f;
        float breadth= 10.0f;
        float expectedPerimeter=40.0f;

        Geometry rectangle=new Rectangle(length,breadth);
        float actualPerimeter=rectangle.calculatePerimeter();

        assertEquals(expectedPerimeter,actualPerimeter,"Wrong Perimeter calculation");

    }
}
