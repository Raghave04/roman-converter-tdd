package com.thoughtworks.vapasi;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SquareTest {


    @Test
    void shouldCalculateAreaOfTheSquare(){
        float side= 5;
        float expectedArea=25;

        Geometry square=new Square(side);
        float actualArea=square.calculateArea();

        assertEquals(expectedArea,actualArea,"Wrong calculation");

    }

    @Test
    void shouldCalculatePerimeterOfTheSquare(){
        float side= 5;
        float expectedArea=25;

        Geometry square=new Square(side);
        float actualArea=square.calculateArea();

        assertEquals(expectedArea,actualArea,"Wrong calculation");

    }


}
