package com.thoughtworks.vapasi;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TossTest {

    @Test
    void shouldCheckForToss() {
        Toss toss=new Toss();

        assertEquals("heads",toss.initializeToss(),"Random Toss");
    }
}
