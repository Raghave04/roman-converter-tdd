package com.thoughtworks.vapasi;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProbabilityTest {

    @Test
    void shouldCheckForProbabilityEvent() {
        double value=0.5;
        Probability probability=new Probability(value,value);
        assertEquals(value,probability.getProbabilityOfAnEvent(),"Probability Failed");
    }

    @Test
    void shouldCheckForCombinedProbabilityEvents() {
        double value1 = 0.5;
        double value2 = 1;
        Probability probability=new Probability(value1,value2);
        assertEquals(0.5,probability.getProbabilityOfTwoCombinedEvents(), "Probability Failed");
    }

    @Test
    void shouldCheckForEitherOneProbabilityEvent() {
        double value1 = 0.5;
        double value2 = 1;
        Probability probability = new Probability(value1,value2);
        assertEquals(1.5, probability.getProbabilityOfEitherOneEvent(), "Probability Failed");
    }

    @Test
    void shouldCheckForEquityOfTwoProbabilities() {
        double value1 = 0.5;
        double value2 = 1;
        Probability probability = new Probability(value1,value2);
        assertEquals(false, probability.getEquityOfTwoEvents(), "Probability Failed");
    }


}

